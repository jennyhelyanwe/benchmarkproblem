gfx read node results region results
gfx read elem results region results

gfx read node benchmark region undeformed
gfx read elem benchmark region undeformed

gfx cre window

gfx modify g_element "/" general clear;
gfx modify g_element "/" points domain_point tessellation default_points LOCAL glyph axes_solid_colour size "1*1*1" offset 0,0,0 font default select_on material default selected_material default_selected render_shaded;
gfx modify g_element /results/ general clear;
gfx modify g_element /results/ lines domain_mesh1d coordinate coordinates tessellation default LOCAL line line_base_size 0 select_on material default selected_material default_selected render_shaded;
gfx modify g_element /results/ points domain_nodes coordinate coordinates tessellation default_points LOCAL glyph point size "1*1*1" offset 0,0,0 font default label cmiss_number label_offset "0,0,0" select_on material default selected_material default_selected render_shaded;
gfx modify g_element /results/ surfaces domain_mesh2d coordinate coordinates face xi3_0 tessellation default LOCAL select_on material default selected_material default_selected render_shaded;
gfx modify g_element /undeformed/ general clear;
gfx modify g_element /undeformed/ lines domain_mesh1d coordinate coordinates tessellation default LOCAL line line_base_size 0 select_on material gold selected_material default_selected render_shaded;


