fem define para;r;benchmark
fem define coor;r;benchmark
fem define base;r;benchmark_trilinear

fem define node;r;benchmark_trilinear
fem define elem;r;benchmark
fem export node;benchmark as Geometry
fem export elem;benchmark as Geometry

fem define fibre;r;benchmark_trilinear
fem define elem;r;benchmark fibre

fem define equa;r;benchmark
fem define mate;r;benchmark

fem define init;r;benchmark_trilinear
fem define solv;r;benchmark

fem solve

fem define init;w;benchmark_deformed
fem export node;results_noupdate field as Deformed_noupdate;
fem export elem;results_noupdate field as Deformed_noupdate;
 
fem update geometry from solution
fem define node;w;results_trilinear 
fem define elem;w;results_trilinear
fem export node;results_trilinear  as Deformed
fem export elem;results_trilinear  as Deformed

