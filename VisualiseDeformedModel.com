gfx read node Results.part0.exnode region deformed
gfx read elem Results.part0.exelem region deformed

gfx read node GeometricField.part0.exnode region undeformed
gfx read elem GeometricField.part0.exelem region undeformed
gfx define faces egroup "undeformed"
gfx define faces egroup "deformed"
gfx cre window

gfx modify g_element "/" general clear;
gfx modify g_element "/" point LOCAL glyph axes_solid_xyz general size "1*1*1" centre 0,0,0 font default select_on material default selected_material default_selected;
gfx modify g_element /deformed/ general clear;
gfx modify g_element /deformed/ lines coordinate DeformedGeometry tessellation default LOCAL select_on material default selected_material default_selected;
gfx modify g_element /deformed/ node_points coordinate DeformedGeometry LOCAL glyph point general size "1*1*1" centre 0,0,0 font default label cmiss_number select_on material default selected_material default_selected;
gfx modify g_element /deformed/ surfaces coordinate DeformedGeometry face xi2_0 tessellation default LOCAL select_on invisible material default selected_material default_selected render_shaded;
gfx modify g_element /undeformed/ general clear;
gfx modify g_element /undeformed/ node_points coordinate Geometry LOCAL glyph point general size "1*1*1" centre 0,0,0 font default select_on material default selected_material default_selected;
gfx modify g_element /undeformed/ lines coordinate Geometry tessellation default LOCAL select_on material gold selected_material default_selected;

