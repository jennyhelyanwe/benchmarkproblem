gfx read node GeometricField.part0.exnode
gfx read elem GeometricField.part0.exelem

gfx cre win
gfx define faces egroup "Region"
gfx edit scene

gfx modify g_element "/" general clear;
gfx modify g_element "/" lines domain_mesh1d coordinate Geometry tessellation default LOCAL line line_base_size 0 select_on material default selected_material default_selected render_shaded;
gfx modify g_element "/" points domain_point tessellation default_points LOCAL glyph axes_solid_colour size "1*1*1" offset 0,0,0 font default select_on material default selected_material default_selected render_shaded;

